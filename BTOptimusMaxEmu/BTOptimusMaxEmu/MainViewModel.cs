﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using Plugin.BLE;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.Exceptions;
using Android.Bluetooth;
using Android.Content;
using Java.Util;

namespace BTOptimusMaxEmu
{
	[DesignTimeVisible(true)]
	class MainViewModel : INotifyPropertyChanged
	{
		[Obsolete]
		public MainViewModel()
		{
			_text = "";
			_activInd = false;
			_deviceList = new ObservableCollection<BluetoothDevice>();
			_selectedDevice = null;
			adapter = BluetoothAdapter.DefaultAdapter;
			_socket = null;
		}

		private string _text;
		private bool _activInd;
		private ObservableCollection<BluetoothDevice> _deviceList;
		private BluetoothDevice _selectedDevice;
		private BluetoothAdapter adapter;
		private BluetoothSocket _socket;

		//-----------------------------------------------------------------------------------------------------------------------

		public BluetoothDevice SelectedDevice
		{
			get => _selectedDevice;
			set
			{
				_selectedDevice = value;
				OnPropertyChanged(nameof(SelectedDevice));
			}
		}

		public ObservableCollection<BluetoothDevice> DeviceList
		{
			get => _deviceList;
			set
			{
				_deviceList = value;
				OnPropertyChanged(nameof(DeviceList));
			}
		}

		public string Text
		{
			get => _text;
			set
			{
				_text = value;
				OnPropertyChanged(nameof(Text));
			}
		}

		public bool ActivInd
		{
			get => _activInd;
			set
			{
				_activInd = value;
				OnPropertyChanged(nameof(ActivInd));
			}
		}


		//================================================================================================================================

		private ICommand _btCommand;
		private ICommand _btLinkCommand;
		private ICommand _btSendCommand;

		//-----------------------------------------------------------------------------------------------------------------------

		public ICommand BTCommand => _btCommand ?? (_btCommand = new RelayCommand(BTCommandExe));
		public ICommand BTLinkCommand => _btLinkCommand ?? (_btLinkCommand = new RelayCommand(BTLinkCommandExe));
		public ICommand BTSendCommand => _btSendCommand ?? (_btSendCommand = new RelayCommand(BTSendCommandExe));


		private async void BTCommandExe()
		{
			Text = "Поиск...";
			ActivInd = true;
			// получить адаптер по-умолчанию

			// проверяем, что у нас получен адаптер (есть Bluetooth) и он включен
			if (adapter != null && adapter.IsEnabled)
			{
				// получим список связанных устройств
				ICollection<BluetoothDevice> devices = adapter.BondedDevices;

				DeviceList.Clear();

				// пройдем по списку устройств и будем заполнять список имен
				foreach (var device in devices)
				{
					// device - сопряженное устройство из BondedDevices
					DeviceList.Add(device); // добавление имени
				}
			}
			Text = "Поиск завершен";
			ActivInd = false;
		}

		private async void BTLinkCommandExe()
		{
			_socket = SelectedDevice.CreateRfcommSocketToServiceRecord(UUID.FromString("00001101-0000-1000-8000-00805f9b34fb"));
			try
			{
				await _socket.ConnectAsync();
			}
			catch (Exception e)
			{
				Text = "Не удалось выполнить соединение";
				throw;
			}
		}

		private async void BTSendCommandExe()
		{
			string msg = "Hello world!";
			var bytemsg = msg.ToCharArray();
			List<byte> buflist = new List<byte>();
			foreach (var c in bytemsg)
			{
				buflist.Add(Convert.ToByte(c));
			}
			await _socket.OutputStream.WriteAsync(buflist.ToArray(), 0, buflist.Count);
		}

		//================================================================================================================================
		public event PropertyChangedEventHandler PropertyChanged;

		private void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
