﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;
using InTheHand.Windows.Forms;

namespace BOME
{
	class Devi
	{
		public string Name { set; get; }
		public int Id { set; get; }
	}
	class MainViewModel : INotifyPropertyChanged
	{
		public MainViewModel()
		{
			_text = "";
			_deviceList = new ObservableCollection<Devi>();
			var D = new Devi();
			D.Name = "test";
			_deviceList.Add(D);
			_deviceList.Add(D);
			_selectedDevice = null;
			client = null;
			peerStream = null;
			_msg = "";
			_listenText = "";
		}

		private string _text;
		private string _msg;
		private string _listenText;
		private ObservableCollection<Devi> _deviceList;
		private Devi _selectedDevice;
		private BluetoothClient client;
		private NetworkStream peerStream;
		

		//---------------------------------------------------------------------------------------------------------------------

		public string listenText
		{
			get { return _listenText; }
			set
			{
				_listenText = value;
				OnPropertyChanged(nameof(listenText));
			}
		}

		public string Text
		{
			get { return _text; }
			set
			{
				_text = value;
				OnPropertyChanged(nameof(Text));
			}
		}

		public string MSG
		{
			get { return _msg; }
			set
			{
				_msg = value;
				OnPropertyChanged(nameof(MSG));
			}
		}

		public ObservableCollection<Devi> DeviceList
		{
			get { return _deviceList; }
			set
			{
				_deviceList = value;
				OnPropertyChanged(nameof(DeviceList));
			}
		}

		public Devi SelectedDevice
		{
			get { return _selectedDevice; }
			set
			{
				_selectedDevice = value;
				OnPropertyChanged(nameof(SelectedDevice));
			}
		}

		//=============================================================================================================================

		private ICommand _browsCommand;
		private ICommand _testCommandCommand;
		private ICommand _listen;

		//-----------------------------------------------------------------------------------------------------------------------

		public ICommand BrowsCommand
		{
			get { return _browsCommand ?? (_browsCommand = new RelayCommand(BrowsCommandExe)); }
		}
		public ICommand TestCommand
		{
			get { return _testCommandCommand ?? (_testCommandCommand = new RelayCommand(TestCommandExe)); }
		}
		public ICommand listen
		{
			get { return _listen ?? (_listen = new RelayCommand<object>(listenexe, canlisten)); }
		}

		private bool canlisten(object arg)
		{
			if (peerStream == null) return true;
			List<byte> bytearr = new List<byte>();
			peerStream.BeginRead(bytearr.ToArray(), 0, 64, new AsyncCallback(TargetMsg), null);
			Thread.Sleep(10000);
			foreach (var b in bytearr)
			{
				listenText += Convert.ToString(b);
			}
			return true;
		}

		private void listenexe(object arg)
		{
			throw new NotImplementedException();
		}

		//------------------------------------------------------------------------------------------------------------------------------------------------------------------


		private async void BrowsCommandExe()
		{
			var serviceClass = BluetoothService.SerialPort;
			if (client != null)
			{
				client.Close();
			}

			client = new BluetoothClient();

			var dlg = new SelectBluetoothDeviceDialog();
			DialogResult result = dlg.ShowDialog();
			if (result != DialogResult.OK)
			{
				return;
			}
			BluetoothDeviceInfo device = dlg.SelectedDevice;
			BluetoothAddress addr = device.DeviceAddress;
			Text = device.DeviceName;

			if (device == null)
			{
				return;
			}

			var ep = new BluetoothEndPoint(addr, serviceClass);

			try
			{
				if (!device.Connected)
				{
					client.Connect(ep);
				}
			}
			catch (System.Net.Sockets.SocketException e)
			{
				client.Close();
				Text = "Не удалось выполнить подключение";
				return;
			}

			Text = "Подключен";
			peerStream = client.GetStream();
		}

		private void TestCommandExe()
		{
			if(peerStream == null) return;
			var msgarr = MSG.ToCharArray();
			List<byte> msgbyte = new List<byte>();
			foreach (var c in msgarr)
			{
				msgbyte.Add(Convert.ToByte(c));
			}

			peerStream.BeginWrite(msgbyte.ToArray(), 0, msgbyte.Count, new AsyncCallback(TargetMsg), null);
		}

		private void TargetMsg(IAsyncResult ar)
		{
			Console.WriteLine(ar.AsyncState);
		}


		//------------------------------------------------------------------------------------------------------------------------------------------------------------------



		//======================================================
		public event PropertyChangedEventHandler PropertyChanged;
		private void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

	}

	//======================================================
	public class RelayCommand : ICommand
	{
		private readonly Action _execute;
		private readonly Func<bool> _canExecute;

		public RelayCommand(Action execute, Func<bool> canExecute = null)
		{
			_execute = execute ?? throw new ArgumentNullException(nameof(execute));
			_canExecute = canExecute;
		}

		public void Execute(object parameter)
		{
			_execute.Invoke();
		}

		public bool CanExecute(object parameter)
		{
			return _canExecute?.Invoke() ?? true;
		}

		public event EventHandler CanExecuteChanged
		{
			add
			{
				CommandManager.RequerySuggested += value;
			}
			remove
			{
				CommandManager.RequerySuggested -= value;
			}
		}
	}

	public class RelayCommand<T> : ICommand
	{
		private readonly Action<T> _execute;
		private readonly Func<T, bool> _canExecute;

		public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
		{
			_execute = execute ?? throw new ArgumentNullException(nameof(execute));
			_canExecute = canExecute;
		}

		public void Execute(object parameter)
		{
			if (parameter is T arg)
			{
				_execute.Invoke(arg);
			}
		}

		public bool CanExecute(object parameter)
		{
			if (parameter is T arg)
			{
				return _canExecute?.Invoke(arg) ?? true;
			}
			return false;
		}

		public event EventHandler CanExecuteChanged
		{
			add
			{
				CommandManager.RequerySuggested += value;
			}
			remove
			{
				CommandManager.RequerySuggested -= value;
			}
		}
	}
}
